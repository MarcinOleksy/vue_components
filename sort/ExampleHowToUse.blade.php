<sort-basic
    :params="{{json_encode($placeSearchForm->getModel())}}"
    url-form-search="{{route('dashboard.places.index')}}"
    inline-template
>
    <table>
        <thead>
            <tr>
                <th><sort name="id" label-column="{{ trans('labels.id') }}"></sort></th>
                <th><sort name="description" label-column="{{ trans('labels.description') }}"></sort></th>
                <th><sort name="created_by" label-column="{{ trans('labels.created_by') }}"></sort></th>
                th><sort name="created_at" label-column="{{ trans('labels.created_at') }}"></sort></th>
                <th></th>
            </tr>
        </thead>
    </table>
</sort-basic>